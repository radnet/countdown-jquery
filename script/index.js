$(function() {

    var date = new Date('2019-12-26 10:45:00');

    $('#clock').countdown(date.getTime())
    .on('update.countdown', function(event) {
        //var format = '%H:%M:%S';
        var format = '%-Hh %Mm %Ss';
        if(event.offset.totalDays > 0) {
            format = '%-D %!D:dzień,dni; ' + format;
        } 
        // else if(event.offset.totalDays > 1) {
        //     format = '%-D dni ' + format;
        // }

        $(this).html(event.strftime(format));
    })
    .on('finish.countdown', function(event) {
        $(this).html('Zakończono')
            .parent().addClass('disabled');

    });
});